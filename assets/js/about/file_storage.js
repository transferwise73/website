/** 
 * ecc.network
 * 
 * @copyright Project ECC
 */

$(function() {
    loadAnimation('#security-animation', '/assets/js/about/file_storage/animation/security.json');
    loadAnimation('#privacy-animation', '/assets/js/about/file_storage/animation/privacy.json');
    loadAnimation('#redundancy-animation', '/assets/js/about/file_storage/animation/redundancy.json');
    loadAnimation('#ease-of-use-animation', '/assets/js/about/file_storage/animation/easy_to_use.json');
    loadAnimation('#host-animation', '/assets/js/about/file_storage/animation/host.json');
});