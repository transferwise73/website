/** 
 * ecc.network
 * 
 * @copyright Project ECC
 */

$(function() {
    loadAnimation('#instant-payments-animation', '/assets/js/about/overview/animation/instant_payments.json');
    loadAnimation('#data-privacy-animation', '/assets/js/about/overview/animation/data_privacy.json');
    loadAnimation('#decentralized-network-animation', '/assets/js/about/overview/animation/decentralized_network.json');
    loadAnimation('#scalable-network-animation', '/assets/js/about/overview/animation/scalable_network.json');

    var getMarketDetails = function() {
        var $btn = $(this)
            .addClass('fa-spin')
            .off('click', getMarketDetails);
        $.getJSON('/about/market_details')
            .done(function(detail) {
                $('.currency').text(detail.currency);
                $('.currency-symbol').text(detail.currencySymbol);

                $('.market-detail.price')
                    .find('.currency-value')
                    .text(detail.price);

                $('.market-detail.market-cap')
                    .find('.currency-value')
                    .text(new Number(detail.marketCap).toLocaleString());

                $('.market-detail.volume')
                    .find('.currency-value')
                    .text(new Number(detail.volume).toLocaleString());
            }).always(function() {
                $btn.removeClass('fa-spin')
                    .on('click', getMarketDetails);
            });
    };
    var $refreshMarketDetailsBtn = $('#refresh-market-details-btn');
    $refreshMarketDetailsBtn
        .click(getMarketDetails)
        .click();

    setInterval(function() {$refreshMarketDetailsBtn.click()}, 30000);
});